#include<GL/glew.h>
#include<GLFW/glfw3.h>
#include<math.h>
#include<cstring>
#include<iostream>
#include <cstdlib> // for sprintf
#define SCREEN_WIDTH 640
#define SCREEN_HEIGHT 480

static void cursorPositionCallback( GLFWwindow *window, double xpos, double ypos );
void cursorEnterCallback( GLFWwindow *window, int entered );
void mouseButtonCallback( GLFWwindow *window, int button, int action, int mods );
void scrollCallback( GLFWwindow *window, double xoffset, double yoffset );

int main( void )
{
    GLFWwindow *window;

    // Initialize the library
    if ( !glfwInit( ) )
    {
        return -1;
    }

    // Create a windowed mode window and its OpenGL context
    window = glfwCreateWindow( SCREEN_WIDTH, SCREEN_HEIGHT, "Cymwiz pipe", NULL, NULL );

    glfwSetCursorPosCallback( window, cursorPositionCallback );
    glfwSetInputMode( window, GLFW_CURSOR, GLFW_CURSOR_NORMAL );

    glfwSetCursorEnterCallback( window, cursorEnterCallback );

    glfwSetMouseButtonCallback( window, mouseButtonCallback );
    glfwSetInputMode( window, GLFW_STICKY_MOUSE_BUTTONS, 1 );

    glfwSetScrollCallback( window, scrollCallback );

    unsigned char pixels[16 * 16 * 4];
    memset( pixels, 0xff, sizeof( pixels ) );
    GLFWimage image;
    image.width = 16;
    image.height = 16;
    image.pixels = pixels;
    GLFWcursor *cursor = glfwCreateCursor( &image, 0, 0 );
    glfwSetCursor( window, cursor ); // set to null to reset cursor

    int screenWidth, screenHeight;
    glfwGetFramebufferSize( window, &screenWidth, &screenHeight );

    if ( !window )
    {
        glfwTerminate( );
        return -1;
    }

    // Make the window's context current
    glfwMakeContextCurrent( window );

    glViewport( 0.0f, 0.0f, screenWidth, screenHeight ); // specifies the part of the window to which OpenGL will draw (in pixels), convert from normalised to pixels
    glMatrixMode( GL_PROJECTION ); // projection matrix defines the properties of the camera that views the objects in the world coordinate frame. Here you typically set the zoom factor, aspect ratio and the near and far clipping planes
    glLoadIdentity( ); // replace the current matrix with the identity matrix and starts us a fresh because matrix transforms such as glOrpho and glRotate cumulate, basically puts us at (0, 0, 0)
    glOrtho( 0, SCREEN_WIDTH, 0, SCREEN_HEIGHT, 0, 1 ); // essentially set coordinate system
    glMatrixMode( GL_MODELVIEW ); // (default matrix mode) modelview matrix defines how your objects are transformed (meaning translation, rotation and scaling) in your world
    glLoadIdentity( ); // same as above comment


    // Loop until the user closes the window
    while ( !glfwWindowShouldClose( window ) )
    {
        glClear( GL_COLOR_BUFFER_BIT );

        // Render OpenGL here

        double xpos, ypos;
        glfwGetCursorPos( window, &xpos, &ypos );

        // Swap front and back buffers
        glfwSwapBuffers( window );

        // Poll for and process events
        glfwPollEvents( );
    }

    glfwTerminate( );

    return 0;
}

static void cursorPositionCallback( GLFWwindow *window, double xpos, double ypos )
{
    char buf[1024];
    GLint windowWidth, windowHeight;
    float xo,yo,xp,yp,Rmin,Rmax,r,L;
    glfwGetWindowSize(window, &windowWidth, &windowHeight);
    Rmin=std::min(windowWidth,windowHeight);
    //Rmax=std::max(windowWidth,windowHeight);
    r=0.5*float(Rmin);
    L=1/r;
    xo=0.5*windowWidth;
    yo=0.5*windowHeight;
    xp=3*L*float(xpos-xo);
    yp=-3*L*float(ypos-yo);
    //std::cout<<"W: "<<windowWidth<<" : "<<"H: "<<windowHeight<<std::endl;
    //std::cout<<"R:"<<r<<std::endl;
    //std::cout << xpos << " : " << ypos << std::endl;
    //if (xp*xp+yp*yp<3.0){ std::cout << xp << " : " << yp << std::endl;}
    if (xp*xp+yp*yp<3.0){
      sprintf(buf,"set handle clamp {%.2f %.2f}\n",xp,yp);
      std::cout << std::string(buf) <<std::endl;
     }

}

void cursorEnterCallback( GLFWwindow *window, int entered )
{
    if ( entered )
    {
        std::cout << "Entered Window" << std::endl;
    }
    else
    {
        std::cout << "Left window" << std::endl;
    }
}

void mouseButtonCallback( GLFWwindow *window, int button, int action, int mods )
{
    if ( button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_PRESS )
    {
        std::cout << "Right button pressed" << std::endl;
    }
}

void scrollCallback( GLFWwindow *window, double xoffset, double yoffset )
{
    std::cout << xoffset << " : " << yoffset << std::endl;
}
