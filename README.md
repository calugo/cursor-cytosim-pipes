# GL MOUSE TRACKER.

This program tracks the mouse coordinates and writes these to a pipe in the form of a cytosim command which moves a particle on running simulation. It requires a fifo and a cytosim file which reads the fifo  in this case cymwiz.  The file runfifo is two commands:

`
../bin/play menu=0 ../cym/cymwiz.cym < fifo&
../../cursor/./cpipe > fifo
` 
Each command launches the respective ends write-read of the fifo. The cursor tracker can be compiled as follows:

`g++ -o cpipe cursorxy.cpp -lglut -lGL -lGLEW -lglfw `

For this test cytosim needs to be compiled in 2d and the option NEW_SOLID_CLAMP set to 1 in the header file solid_prop.h 
